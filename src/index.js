import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
// import Hello from './components/hello'
import Flat from "./components/flat";
ReactDOM.render(
  <App />,
  document.getElementById('root')
);
